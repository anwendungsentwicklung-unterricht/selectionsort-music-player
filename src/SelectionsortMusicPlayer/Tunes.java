package SelectionsortMusicPlayer;

public class Tunes  {
    public static void main(String[] args) {
        Album music = new Album();

        music.addTitle("By the Way", "Red Hot Chili Peppers", 6);
        music.addTitle("Come On Over", "Shania Twain", 5);
        music.addTitle("Soundtrack", "The Producers", 4);
        music.addTitle("Play", "Jennifer Lopez", 3);


        music.addTitle("Double Live", "Garth Brooks", 2);
        music.addTitle("Greatest Hits", "Stone Temple Pilots", 1);

        System.out.println(music);


        music.setList(Album.SelectionSort(music.getList()));
        System.out.println(music);
    }
}
