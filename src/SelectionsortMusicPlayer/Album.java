package SelectionsortMusicPlayer;

import java.util.ArrayList;

public class Album {
    private ArrayList<Title> list = new ArrayList<Title>();

    public void addTitle(String title, String singer, int price) {
        Title item = new Title(title, singer, price);
        this.list.add(item);
    }

    public ArrayList<Title> getList() {
        return this.list;
    }

    public void setList(ArrayList<Title> arrayList) {
        this.list = arrayList;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Title title : list) {
            result.append(", ").append(((Title) title).getTitle());
        }

        return result.toString();
    }

    public static ArrayList<Title> SelectionSort(ArrayList<Title> arrayList)
    {
        for (int i = 0; i < arrayList.size(); i++)
        {
            int minPos = i;
            for (int j = i + 1; j < arrayList.size(); j++)
            {
                if (arrayList.get(j).getSongLength() < arrayList.get(minPos).getSongLength())
                {
                    minPos = j;
                }
            }
            Title tmpTitle = arrayList.get(minPos);

            arrayList.set(minPos, arrayList.get(i));
            arrayList.set(i, tmpTitle);
        }
        return arrayList;
    }
}
